<?php
/**
  * Redis configuration settings for WordPress
  */

use Roots\WPConfig\Config;

Config::define('WP_REDIS_HOST', getenv('WP_REDIS_HOST'));
Config::define('WP_CACHE_KEY_SALT', getenv('WP_CACHE_KEY_SALT'));
