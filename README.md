# [Bedrock](https://roots.io/bedrock/) + Docker

> Bedrock is a modern WordPress stack that helps you get started with the best development tools and project structure.
 Much of the philosophy behind Bedrock is inspired by the [Twelve-Factor App](http://12factor.net/) methodology including the [WordPress specific version](https://roots.io/twelve-factor-wordpress/).

This repository is a starter package for modern WordPress development. 

## Features

* PHP-FPM
* nginx, plug-and-play support for [nginx-proxy](https://github.com/jwilder/nginx-proxy) & [Let's Encrypt SSL](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) for production use 
* Redis cache preconfigured
* WP-CLI
* Bedrock

**The usual Bedrock features:** 
* Better folder structure
* Dependency management with [Composer](https://getcomposer.org)
* Easy WordPress configuration with environment specific files
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)
* Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))

## Requirements

* Docker and Docker Compose
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) or use the [official Docker image](https://hub.docker.com/_/composer) with the following command:
 
```sh
docker run --rm -t --volume $(pwd):/app --user 1000:1000 --volume /tmp:/tmp composer
```

> Make sure to adjust `UID` and `GID` to your user to avoid permission issues. 

## Installation

1. Pull the repo:

    ```sh
    git clone https://gitlab.com/webmenedzser/wordpress_boilerplate.git
    ```
2. Make a copy of .env.example: `cp .env.example .env` 
3. Update environment variables in the `.env` file:
  * `COMPOSE_PROJECT_NAME` - a prefix for Docker containers
  * `VIRTUAL_HOST` - The URL to be proxied by [nginx-proxy](https://github.com/jwilder/nginx-proxy). Certs generated by [letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) will contain these. For more details see [this post](https://www.wbmngr.agency/blog/docker-based-hosting-on-your-own-vps). 
  * Database variables
    * `DB_NAME` - Database name
    * `DB_USER` - Database user
    * `DB_PASSWORD` - Database password
    * `DB_ROOT_PASSWORD` - Database root password
    * `DB_HOST` - Database host
    * `DB_PREFIX` - WordPress DB prefix
    * Optionally, you can define `DATABASE_URL` for using a DSN instead of using the variables above (e.g. `mysql://user:password@127.0.0.1:3306/db_name`)
  * WordPress variables
      * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
      * `WP_HOME` - Full URL to WordPress home (https://example.com)
      * `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)
      * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`, `WP_CACHE_SALT_KEY`
        * Generate with [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command)
        * Generate with [our WordPress salts generator](https://roots.io/salts.html)
        * `WP_CACHE_SALT_KEY` is used by Redis
4. Start the containers: `docker-compose up -d`
5. Install the requirements: `composer install`
6. Navigate to `http://${COMPOSE_PROJECT_NAME}.localhost:3000` (so if `COMPOSE_PROJECT_NAME` is `trololo`, then your `WP_HOME` env variable would be `http://trololo.localhost:3000`)
7. Set up WordPress and log in
8. Navigate to Plugins and activate the plugins you need (Wordfence Security is automatically disabled in `development` mode)
9. Enable Redis at `Settings > Redis` (click on **Enable Object Cache** button). (*Note:* Redis cache is enabled by default. If you experience problems with it, just delete `web/app/object-cache.php`) 
10. Delete the *mu-plugin* you don't need from `web/app/mu-plugins` (`bedrock-autoloader.php`, `disallow-indexing.php` and `register-theme-directory.php` should stay there)
11. Add theme(s) in `web/app/themes/` as you would for a normal WordPress site
12. Install plugins from [WPackagist.org](https://wpackagist.org) with Composer

## Documentation

Bedrock documentation is available at [https://roots.io/bedrock/docs/](https://roots.io/bedrock/docs/).

### Install a plugin

You should install plugins with Composer. WordPress plugins and themes are available at [WPackagist.org](https://wpackagist.org/). 

```sh
composer require wpackagist-plugin/woocommerce
```

You can use the `composer.json` file to lock a specific version of a plugin. Just modify the line in `composer.json`, e.g.: 

```json
    "wpackagist-plugin/woocommerce": "3.4.3"
```

Running `composer update` will update the `composer.lock` file with the new requirements.
You can test your rules [here](https://jubianchi.github.io/semver-check/).

#### Packages not available in WPackagist.org repository

If the desired package is not available in the repository (e.g. because it is a paid plugin), you can just copy it to `web/app/plugins`. Make sure to add a new line to `.gitignore` to include that folder in the repo. For example: ACF PRO is not available at WPackagist.org. Just install it as you would normally and add the following line to the `.gitignore` file: 

```git
!web/app/plugins/advanced-custom-fields-pro
```

### WP-CLI usage

You can run WP-CLI with the following command: 

```sh
docker-compose run wpcli wp
```

There is a `wp` script in npm scripts, so you can use it this way, too: 

```bash
npm run wp 
```

> Important note: if WP-CLI returns with results using `less` (e.g. `npm run wp --info`), then you might experience an error message similar to this: 

```bash
less: unrecognized option: r
BusyBox v1.29.3 (2019-01-24 07:45:07 UTC) multi-call binary.

Usage: less [-EFIMmNSRh~] [FILE]...

View FILE (or stdin) one screenful at a time

        -E      Quit once the end of a file is reached
        -F      Quit if entire file fits on first screen
        -I      Ignore case in all searches
        -M,-m   Display status line with line numbers
                and percentage through the file
        -N      Prefix line number to each line
        -S      Truncate long lines
        -R      Remove color escape codes in input
        -~      Suppress ~s displayed past EOF
```

In this case use the `--` parameter after `npm run wp` (like `npm run wp -- --info`) or fallback to `docker-compose run wpcli wp`. 

### Database export & import

There are scripts in `package.json` to ease the DB dumping and importing from CLI.

#### Import
These scripts will import standard `.sql` and gzipped sql (`.sql.gz`) file (`.docker/dump/base.sql` or `.docker/dump/base.sql.gz`) with the following commands: 

```bash
npm run db:import
``` 

OR 

```bash
npm run db:importgz
```

#### Export 

You can easily export your current DB to `.docker/dump/base.sql` with the following command: 

```bash
npm run db:export
``` 

## Contributing

Contributions are welcome from everyone. 
