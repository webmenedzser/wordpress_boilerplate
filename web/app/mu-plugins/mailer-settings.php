<?php
/*
Plugin Name: SMTP settings plugin
Plugin URI: https://www.webmenedzser.hu
Description: Configure SMTP settings via code
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

function configureMailer($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = getenv('SMTP_HOST');
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = getenv('SMTP_PORT');
    $phpmailer->Username = getenv('SMTP_USERNAME');
    $phpmailer->Password = getenv('SMTP_PASSWORD');
}
add_action('phpmailer_init', 'configureMailer');

add_filter( 'wp_mail_from', function( $email ) {
    return 'otto@webmenedzser.hu';
});
