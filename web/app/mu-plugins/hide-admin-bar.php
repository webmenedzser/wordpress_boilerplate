<?php
/*
Plugin Name: Hide WordPress admin bar
Plugin URI: https://www.webmenedzser.hu
Description: Hide the WordPress admin bar on the frontend
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

/*
 * Disable WordPress Admin Bar for all users.
 */
show_admin_bar(false);
