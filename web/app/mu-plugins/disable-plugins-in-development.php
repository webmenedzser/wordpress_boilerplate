<?php
/*
Plugin Name: Disable plugins
Plugin URI: https://www.webmenedzser.hu
Description: Automatically disable plugins in development mode
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

if (WP_ENV === 'development') {
    require_once(ABSPATH . 'wp-admin/includes/plugin.php');

    // You can extend this array with other plugins
    // Wordfence is here as an example
    $plugins = [
        'wordfence/wordfence.php',
    ];

    deactivate_plugins($plugins);
}
