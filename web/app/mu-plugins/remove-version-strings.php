<?php
/*
Plugin Name: Remove query strings from static resources
Plugin URI: https://www.webmenedzser.hu
Description: Remove query strings from static resources in WordPress
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

/*
 * Remove WP Version From Styles
 */
add_filter( 'style_loader_src', 'remove_res_version', 9999 );

/*
 * Remove WP Version From Scripts
 */
add_filter( 'script_loader_src', 'remove_res_version', 9999 );

/*
 * Remove version number strings from static resources
 */
function remove_res_version( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
