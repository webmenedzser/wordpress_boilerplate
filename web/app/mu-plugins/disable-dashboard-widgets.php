<?php
/*
Plugin Name: Remove WP Dashboard Widgets plugin
Plugin URI: https://www.webmenedzser.hu
Description: Disable unnecessary Dashboard Widgets
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

function remove_dashboard_widgets() {
    global $wp_meta_boxes;

    // Gyors vázlat
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);

    // Történések
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);

    // Jelenleg...
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);

    // WordPress Események és Hírek
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);

    // Üdvözlet
    remove_action('welcome_panel', 'wp_welcome_panel');
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
