<?php
/*
Plugin Name: Hide WP generator tag
Plugin URI: http://webmenedzser.hu
Description: Hide WP generator tag
Version: 1.0
Author: Radics Ottó
Author URI: http://webmenedzser.hu
License: GPLv3
*/

remove_action('wp_head', 'wp_generator');
