<?php
/*
Plugin Name: Replace footer text
Plugin URI: https://www.webmenedzser.hu
Description: This plugin helps you change the "Thank You for Creating with WordPress" text in the footer.
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

add_filter('admin_footer_text', '__return_false');
