<?php
/*
Plugin Name: Load custom CSS for WP admin plugin
Plugin URI: https://www.webmenedzser.hu
Description: Load custom CSS for WP admin plugin
Version: 1.0
Author: Radics Ottó
Author URI: https://www.webmenedzser.hu
License: GPLv3
*/

function disable_shake_js_login_head() {
    remove_action('login_head', 'wp_shake_js', 12);
}
add_action('login_head', 'disable_shake_js_login_head');

function override_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'override_login_logo_url' );

function override_login_logo_url_title() {
    return get_bloginfo( 'description' );
}
add_filter( 'login_headertext', 'override_login_logo_url_title' );

function override_login_logo_image() {
    // Change to your logo
    echo '<style>.login h1 a {background-image: url("https://www.webmenedzser.hu/assets/dist/images/webmngr-logo-gradient.svg")}</style>';
};
add_action('login_head', 'override_login_logo_image');
